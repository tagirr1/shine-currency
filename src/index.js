// import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'
import { AppContainer } from 'react-hot-loader'
import configureStore from './store/configureStore'
import './styles/app.css'
import { Provider } from 'react-redux'

const store = configureStore()

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <div className='app'>
          <Component />
        </div>
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  )
}

render(App)

if (module.hot) {
  module.hot.accept('./containers/App', () => { render(App) })
}
