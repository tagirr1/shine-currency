const initialState = {list: [], length: 0}

export default function transactions(state = initialState, action) {

  const old_length= state.length
  const old_array = state.list

  switch(action.type){
    case 'NEW_TRANSACTION':
      return { list: old_array.concat(action.list), length: old_length+1}
    default:
      return state;
  }

}
