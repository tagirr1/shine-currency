import React, { Component } from 'react'
import Project from '../components/Project'

export default class Theme extends Component {
  render() {
    const { theme_id, total, projects } = this.props
    console.log(theme_id, total, projects)
    let projectsList = []
    let j = 0
    for (let i in projects) {

      projectsList.push(
        <Project total={projects[i].count} key={j} name={i} />
      )
      j++
    }


    return <div className='theme'>
      <b>Theme {theme_id} total money ${total}</b>
      {projectsList}
    </div>
  }
}

// Theme.propTypes = {
//   theme_id: PropTypes.number.isRequired,
//   total: PropTypes.number.isRequired,
//   projects: PropTypes.object.isRequired
// }
