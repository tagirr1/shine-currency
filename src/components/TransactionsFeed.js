import React, {Component } from 'react'
import PropTypes from 'prop-types';
import TransItem from '../components/TransItem'

export default class TransactionsFeed extends Component {

  render() {

    const { transactions } = this.props
    let transactionList = []

    for (let data of transactions.list) {
      transactionList.push(
        <TransItem id={data.id} key={data.id} name={data.name} info={data}/>
      )
    }

    return (
      <div className=''>
        <h3>Transaction feed | total {transactions.length}</h3>
        <div className='transactionFeed'>
        {transactionList}
        </div>
      </div>
    )

  }
}

TransactionsFeed.propTypes = {
  transactions: PropTypes.object.isRequired,
}
