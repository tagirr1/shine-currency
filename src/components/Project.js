import React, { Component } from 'react'

export default class Project extends Component {
  render() {
    const { name , total} = this.props


    return <div className='project'>
      <p>Project  {name} ${total}</p>

    </div>
  }
}

// Theme.propTypes = {
//   theme_id: PropTypes.number.isRequired,
//   total: PropTypes.number.isRequired,
//   projects: PropTypes.object.isRequired
// }
