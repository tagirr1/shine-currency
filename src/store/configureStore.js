import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'
import remoteActionMiddleware from './enhancers/ping' // <!-- подключаем наш enhancer
import thunk from 'redux-thunk'

//import createSocketIoMiddleware from 'redux-socket.io';

import {socket} from '../socket.js'


export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(remoteActionMiddleware(socket), thunk)
  )

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers')
      store.replaceReducer(nextRootReducer)
    })
  }


  return store
}
